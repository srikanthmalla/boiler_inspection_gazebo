#include <ros/ros.h>
#include "Greedyplanner.h"
#include <math.h>
#include "Markers.h"
geometry_msgs::PoseArray waypoints,dummy_msg;
geometry_msgs::PoseArray target;

ros::Publisher pub_waypointlist, waypoint_marker_pub,path_marker_pub;
visualization_msgs::Marker waypoints_marker,path_marker;
void GetOdom(const nav_msgs::Odometry::ConstPtr &msg)
{
	geometry_msgs::Pose ps;
	nav_msgs::Odometry curr_pose = *msg;
	ps.position = curr_pose.pose.pose.position;
	ps.orientation =curr_pose.pose.pose.orientation;
	if(waypoints.poses.size()>1)
	{
		float error=sqrt(pow((waypoints.poses.at(0).position.x-ps.position.x),2)+pow((waypoints.poses.at(0).position.y-ps.position.y),2)+ pow((waypoints.poses.at(0).position.z-ps.position.z),2));
		if(error<0.3)
		{
			waypoints.poses.erase(waypoints.poses.begin());
		}
	}
target.poses.clear();
target.poses.push_back(waypoints.poses.at(0));
if(waypoints.poses.size()==1)
{
    float error=sqrt(pow((waypoints.poses.at(0).position.x-ps.position.x),2)+pow((waypoints.poses.at(0).position.y-ps.position.y),2)+ pow((waypoints.poses.at(0).position.z-ps.position.z),2));
    if(error<0.3)
    {
 
	waypoints.poses.clear();
	for(int i=0;i<dummy_msg.poses.size();i++)
	{
	waypoints.poses.push_back(dummy_msg.poses.at(i));
	}
    }

}
pub_waypointlist.publish(target);
waypoint_marker_pub.publish(waypoints_marker);
path_marker_pub.publish(path_marker);
}


int main (int argc, char** argv)
{
  ros::init (argc, argv, "demo");
  ros::NodeHandle nh;

  pub_waypointlist= nh.advertise<geometry_msgs::PoseArray> ("waypoints", 10);
  //ros::NodeHandle nh;
  Greedyplanner gp;

  Markers marker;
  marker.points(waypoints_marker);
  marker.line_list(path_marker);
  waypoint_marker_pub= nh.advertise<visualization_msgs::Marker> ("waypoint_marker", 10);
  path_marker_pub= nh.advertise<visualization_msgs::Marker> ("path_marker", 10);

  ros::Subscriber odometry_sub = nh.subscribe<nav_msgs::Odometry>("lidar_ekf/imu_odom", 10, GetOdom);
  //give trajectoris to the waypoints 
  geometry_msgs::Pose ps;geometry_msgs::Point waypoint; waypoints_marker.color.a=1;waypoints_marker.color.b=1;
 
  ps.position.x=0; 
  ps.position.y=0.8;
  ps.position.z=-1.0; 
  ps.orientation.z=0; waypoints.poses.push_back(ps);  
  waypoint.x=ps.position.x;waypoint.y=ps.position.y;waypoint.z=ps.position.z;
waypoints_marker.points.push_back(waypoint);
 
//ps.position.x=0;
//  ps.position.y=0; 
//  ps.position.z=-1.0; 
//  ps.orientation.z=0; waypoints.poses.push_back(ps);
//  waypoint.x=ps.position.x;waypoint.y=ps.position.y;waypoint.z=ps.position.z;
//waypoints_marker.points.push_back(waypoint);
 
// ps.position.x=0;
//  ps.position.y=0; 
//  ps.position.z=-1.0; 
//  ps.orientation.z=0; waypoints.poses.push_back(ps);
//  waypoint.x=ps.position.x;waypoint.y=ps.position.y;waypoint.z=ps.position.z;
//waypoints_marker.points.push_back(waypoint);

 ps.position.x=0;
  ps.position.y=-1.7; 
  ps.position.z=-1.0; 
  ps.orientation.z=0; waypoints.poses.push_back(ps);
  waypoint.x=ps.position.x;waypoint.y=ps.position.y;waypoint.z=ps.position.z;
waypoints_marker.points.push_back(waypoint);  

ps.position.x=0;
  ps.position.y=-1.7; 
  ps.position.z=-1.5; 
  ps.orientation.z=0; waypoints.poses.push_back(ps);
  waypoint.x=ps.position.x;waypoint.y=ps.position.y;waypoint.z=ps.position.z;
waypoints_marker.points.push_back(waypoint);  

//  ps.position.x=0; 
//  ps.position.y=-0.6; 
//  ps.position.z=-2; 
//  ps.orientation.z=0; waypoints.poses.push_back(ps);
//  waypoint.x=ps.position.x;waypoint.y=ps.position.y;waypoint.z=ps.position.z;
//waypoints_marker.points.push_back(waypoint);

//  ps.position.x=0; 
//  ps.position.y=0; 
//  ps.position.z=-2; 
//  ps.orientation.z=0; waypoints.poses.push_back(ps);
//  waypoint.x=ps.position.x;waypoint.y=ps.position.y;waypoint.z=ps.position.z;
//waypoints_marker.points.push_back(waypoint);
  
//  ps.position.x=0; 
//  ps.position.y=0.6; 
//  ps.position.z=-2; 
//  ps.orientation.z=0; waypoints.poses.push_back(ps);
//  waypoint.x=ps.position.x;waypoint.y=ps.position.y;waypoint.z=ps.position.z;
//waypoints_marker.points.push_back(waypoint);
  
  ps.position.x=0; 
  ps.position.y=0.8; 
  ps.position.z=-1.5; 
  ps.orientation.z=0; waypoints.poses.push_back(ps);
  waypoint.x=ps.position.x;waypoint.y=ps.position.y;waypoint.z=ps.position.z;
waypoints_marker.points.push_back(waypoint);  

 ps.position.x=0;
  ps.position.y=0.8; 
  ps.position.z=-2; 
  ps.orientation.z=0; waypoints.poses.push_back(ps);
  waypoint.x=ps.position.x;waypoint.y=ps.position.y;waypoint.z=ps.position.z;
waypoints_marker.points.push_back(waypoint);

//  ps.position.x=0; 
//  ps.position.y=0.6;
//  ps.position.z=-3.0; 
//  ps.orientation.z=0; waypoints.poses.push_back(ps);  
//  waypoint.x=ps.position.x;waypoint.y=ps.position.y;waypoint.z=ps.position.z;
//waypoints_marker.points.push_back(waypoint);

// ps.position.x=0;
//  ps.position.y=0; 
//  ps.position.z=-3.0; 
//  ps.orientation.z=0; waypoints.poses.push_back(ps);
//  waypoint.x=ps.position.x;waypoint.y=ps.position.y;waypoint.z=ps.position.z;
//waypoints_marker.points.push_back(waypoint);

//  ps.position.x=0; 
//  ps.position.y=-0.6;
//  ps.position.z=-3.0; 
//  ps.orientation.z=0; waypoints.poses.push_back(ps);  
//  waypoint.x=ps.position.x;waypoint.y=ps.position.y;waypoint.z=ps.position.z;
//waypoints_marker.points.push_back(waypoint);

  ps.position.x=0; 
  ps.position.y=-1.7;
  ps.position.z=-2.0; 
  ps.orientation.z=0; waypoints.poses.push_back(ps);  
  waypoint.x=ps.position.x;waypoint.y=ps.position.y;waypoint.z=ps.position.z;
waypoints_marker.points.push_back(waypoint);


ps.position.x=0;
  ps.position.y=0; 
  ps.position.z=-0.5; 
  ps.orientation.z=0; waypoints.poses.push_back(ps);
  waypoint.x=ps.position.x;waypoint.y=ps.position.y;waypoint.z=ps.position.z;
waypoints_marker.points.push_back(waypoint);
  //ps.position.x=-0.5; ps.position.y=0; ps.position.z=2; ps.orientation.z=0; waypoints.poses.push_back(ps);
  //waypoint.x=ps.position.x;waypoint.y=ps.position.y;waypoint.z=ps.position.z;waypoints_marker.points.push_back(waypoint);
  ros::Rate loop_rate(10.0);

  path_marker.color.a=1;path_marker.color.g=1;
  for(int i=0;i<waypoints.poses.size()-1;i++)
  {
    path_marker.points.push_back(waypoints_marker.points.at(i));
    path_marker.points.push_back(waypoints_marker.points.at(i+1));
  }
  dummy_msg.poses.clear();
        for(int i=0;i<waypoints.poses.size();i++)
        {
           dummy_msg.poses.push_back(waypoints.poses.at(i));
        }

  while(ros::ok())
  {   
    ros::spinOnce();
    loop_rate.sleep();
  }


}
